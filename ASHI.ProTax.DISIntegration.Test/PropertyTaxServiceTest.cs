﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASHI.ProTax.DISIntegration.Interfaces;
using ASHI.ProTax.DISIntegration.Services;

namespace ASHI.ProTax.DISIntegration.Test
{
    [TestClass]
    public class PropertyTaxServiceTest
    {
        private string ServiceUrl { get; set; }

        [TestInitialize]
        public void SetUp()
        {
            this.ServiceUrl = "https://test.protax2.org/protax/EServiseServlet";
        }
        [TestMethod]
        [Description("Ensures that a document is always returned if the supplied data is correct")]
        public void ShouldReturnADocumentIfDataIsValid()
        {
            IPropertyTaxService service = new PropertyTaxService(this.ServiceUrl);


            var result = service.GetPropertyTaxBill("1234567890", "Besa", "Gashi");

            Assert.IsTrue(result.IsSuccess);
            Assert.IsNotNull(result.Document);

            //System.IO.File.WriteAllBytes("test.pdf", Convert.FromBase64String(result.Document));

        }
        [TestMethod]
        [Description("Ensures that the service method returns an error if data is not valid")]
        public void ShouldReturnAnErrorIfDataIsNotValid()
        {
            IPropertyTaxService service = new PropertyTaxService(this.ServiceUrl);


            var result = service.GetPropertyTaxBill("000", "Besa", "Gashi");

            Assert.IsFalse(result.IsSuccess);
        }

        [TestMethod]
        [Description("Ensures that the service method returns an error if not all parameters are supplied")]
        public void ShouldReturnAnErrorIfInputParametersMissingIsNotValid()
        {
            IPropertyTaxService service = new PropertyTaxService(this.ServiceUrl);


            var result = service.GetPropertyTaxBill(null, "Besa", "Gashi");

            Assert.IsFalse(result.IsSuccess);
        }
    }
}
