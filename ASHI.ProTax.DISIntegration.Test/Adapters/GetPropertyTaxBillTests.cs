﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASHI.ProTax.DISIntegration.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using Neurodot.DIS.Helpers;

namespace ASHI.ProTax.DISIntegration.Adapters.Tests
{
    [TestClass()]
    public class GetPropertyTaxBillTests
    {
        private XmlDocument document { get; set; }
        private DotNetAdapter adapter { get; set; }

        [TestInitialize]
        public void SetUp()
        {
            this.document = new XmlDocument();
            document.LoadXml(File.ReadAllText("sampleRequest.xml"));

            this.adapter = new GetPropertyTaxBillAdapter();
        }
        // Make sure to test XML Structure


        [TestMethod()]
        public void ShouldReturnPayloadAsPartOfTheXmlDocument()
        {
            var response = this.adapter.Execute(this.document);

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(response.NameTable);
            nsmgr.AddNamespace("gt", "http://www.govtalk.gov.uk/CM/envelope");
            nsmgr.AddNamespace("ptb", "urn:mf:response:propertytaxbill");
            nsmgr.AddNamespace("e", "urn:env:response:v1");


            XmlNode qualifier = response.SelectSingleNode("/gt:GovTalkMessage/gt:Header/gt:MessageDetails/gt:Qualifier", nsmgr);
            string payload = response.SelectSingleNode("/gt:GovTalkMessage/gt:Body/e:Message/e:Data/ptb:PropertyTaxBillResponse/ptb:PropertyTaxBill/ptb:Payload", nsmgr)?.InnerText;

            //System.IO.File.WriteAllBytes("test.pdf", Convert.FromBase64String(payload));

            Assert.IsTrue(qualifier.InnerText == "response");
            Assert.IsTrue(!string.IsNullOrEmpty(payload));
        }

        [TestMethod()]
        public void ShouldReturnAnXmlErrorNodeIfParameterNotSupplied()
        {
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(this.document.NameTable);
            nsmgr.AddNamespace("gt", "http://www.govtalk.gov.uk/CM/envelope");
            nsmgr.AddNamespace("r", "urn:env:request:v1");
            nsmgr.AddNamespace("ptb", "urn:mf:request:propertytaxbill");
            nsmgr.AddNamespace("ptb2", "urn:mf:response:propertytaxbill");
            nsmgr.AddNamespace("pte", "urn:mf:response:propertytaxbillerror");

            nsmgr.AddNamespace("e", "urn:env:response:v1");

            var request = this.document.SelectSingleNode("/gt:GovTalkMessage/gt:Body/r:Message/r:Data/ptb:PropertyTaxBillRequest", nsmgr);

            // Simulate not supplying a personal number
            request.RemoveChild(request.SelectSingleNode("ptb:PersonalNumber", nsmgr));


            var response = this.adapter.Execute(this.document);

            var errorNode = response.SelectSingleNode("/gt:GovTalkMessage/gt:Body/e:Message/e:Data/pte:PropertyTaxBillRequestError", nsmgr);

            Assert.IsNotNull(errorNode);
        }
        [TestMethod()]
        public void ShouldReturnAnXmlErrorNodeIfMalformedXmlStructure()
        {
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(this.document.NameTable);
            nsmgr.AddNamespace("gt", "http://www.govtalk.gov.uk/CM/envelope");
            nsmgr.AddNamespace("r", "urn:env:request:v1");
            nsmgr.AddNamespace("ptb", "urn:mf:request:propertytaxbill");
            nsmgr.AddNamespace("ptb2", "urn:mf:response:propertytaxbill");
            nsmgr.AddNamespace("pte", "urn:mf:response:propertytaxbillerror");

            nsmgr.AddNamespace("e", "urn:env:response:v1");

            
            var request = this.document.SelectSingleNode("/gt:GovTalkMessage/gt:Body/r:Message/r:Data", nsmgr);
            
            // Remove a required node
            request.RemoveChild(request.SelectSingleNode("ptb:PropertyTaxBillRequest", nsmgr));
        
            var response = this.adapter.Execute(this.document);

            var errorNode = response.SelectSingleNode("/gt:GovTalkMessage/gt:Body/e:Message/e:Data/pte:PropertyTaxBillRequestError", nsmgr);

            Assert.IsNotNull(errorNode);
        }
    }
}