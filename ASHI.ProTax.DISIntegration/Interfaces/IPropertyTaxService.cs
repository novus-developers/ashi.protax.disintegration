﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASHI.ProTax.DISIntegration.Interfaces
{
    public interface IPropertyTaxService
    {
        PropertyTaxBillResult GetPropertyTaxBill(string personalNumber, string firstName, string lastName);
    }
}
