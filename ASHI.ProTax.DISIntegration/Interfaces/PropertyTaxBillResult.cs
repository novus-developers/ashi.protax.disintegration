﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASHI.ProTax.DISIntegration.Interfaces
{
    public class PropertyTaxBillResult
    {
        public string Document { get; set; }
        public IList<string> Errors { get; set; }

        public PropertyTaxBillResult()
        {
            this.Errors = new List<string>();
        }

        public bool IsSuccess
        {
            get
            {
                return this.Errors.Count() == 0;
            }
        }

        public static PropertyTaxBillResult Error(string error)
        {
            var res = new PropertyTaxBillResult();

            res.Errors.Add(error);
            return res;
        }
    }
}
