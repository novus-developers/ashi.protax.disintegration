﻿using ASHI.ProTax.DISIntegration.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ASHI.ProTax.DISIntegration.Services
{
    public class PropertyTaxService : IPropertyTaxService
    {
        private string ServiceUrl { get; set; }
        public PropertyTaxService(string serviceUrl)
        {
            this.ServiceUrl = serviceUrl;
        }
        public PropertyTaxBillResult GetPropertyTaxBill(string personalNumber, string firstName, string lastName)
        {
            if (string.IsNullOrEmpty(personalNumber) || string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName) || (personalNumber != null && personalNumber.Length != 10))
                return PropertyTaxBillResult.Error("Please check your input parameters");


            var builder = new UriBuilder(this.ServiceUrl);
            builder.Query = string.Join("&", new string[] { $"personalnumber={personalNumber}", $"firstname={firstName}", $"lastname={lastName}", $"ekiosk=true" });

            HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(builder.Uri);
            httpRequest.Method = WebRequestMethods.Http.Get;

            HttpWebResponse httpResponse = (HttpWebResponse)httpRequest.GetResponse();
            Stream httpResponseStream = httpResponse.GetResponseStream();

            if (httpResponse.Headers["validParameters"] != null && httpResponse.Headers["validParameters"] == "validBill")
            {
                byte[] bytes;
                using (var memoryStream = new MemoryStream())
                {
                    httpResponseStream.CopyTo(memoryStream);
                    bytes = memoryStream.ToArray();
                }

                string fileAsBase64 = Convert.ToBase64String(bytes);

                return new PropertyTaxBillResult
                {
                    Document = fileAsBase64
                };

            }

            return PropertyTaxBillResult.Error("Please check your input parameters");
        }

    }
}
