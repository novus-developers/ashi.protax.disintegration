﻿using ASHI.ProTax.DISIntegration.Interfaces;
using ASHI.ProTax.DISIntegration.Services;
using Microsoft.CGG.Logging;
using Neurodot.DIS.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ASHI.ProTax.DISIntegration.Adapters
{
    public class GetPropertyTaxBillAdapter : DotNetAdapter
    {
        private IPropertyTaxService service { get; set; }

        public GetPropertyTaxBillAdapter()
        {
            var serviceUrl = "https://test.protax2.org/protax/EServiseServlet";

#if RELEASE
            serviceUrl = Microsoft.CGG.Configuration.CGGConfiguration.GetParameterValueAsString("ProTaxServiceUrl", "ASHI");
#endif

            this.service = new PropertyTaxService(serviceUrl);
        }

        public XmlDocument Execute(XmlDocument inMsg)
        {
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(inMsg.NameTable);
            nsmgr.AddNamespace("gt", "http://www.govtalk.gov.uk/CM/envelope");
            nsmgr.AddNamespace("r", "urn:env:request:v1");
            nsmgr.AddNamespace("ptb", "urn:mf:request:propertytaxbill");
            var correlationId = inMsg.SelectSingleNode("/gt:GovTalkMessage/gt:Header/gt:MessageDetails/gt:CorrelationID", nsmgr)?.InnerText;
            var serviceClass = inMsg.SelectSingleNode("/gt:GovTalkMessage/gt:Header/gt:MessageDetails/gt:Class", nsmgr)?.InnerText;

            XmlNode requestBody = inMsg.SelectSingleNode("/gt:GovTalkMessage/gt:Body", nsmgr);
            inMsg.SelectSingleNode("/gt:GovTalkMessage/gt:Header/gt:MessageDetails/gt:Qualifier", nsmgr).InnerText = "response";

            XmlNode request = inMsg.SelectSingleNode("/gt:GovTalkMessage/gt:Body/r:Message/r:Data/ptb:PropertyTaxBillRequest", nsmgr);

            // ensure correct xml structure

            if (request == null)
            {
                requestBody.InnerXml = BuildFailureXml(PropertyTaxBillResult.Error("Malformed XML structure")).OuterXml;

                return inMsg;
            }
            try
            {
                string personalNumber = request.SelectSingleNode("ptb:PersonalNumber", nsmgr)?.InnerText;
                string firstName = request.SelectSingleNode("ptb:FirstName", nsmgr)?.InnerText;
                string lastName = request.SelectSingleNode("ptb:LastName", nsmgr)?.InnerText;

                var taxBillResult = this.service.GetPropertyTaxBill(personalNumber, firstName, lastName);
                requestBody.InnerXml = ToXmlResponse(taxBillResult).OuterXml;

            }
            catch (Exception)
            {
                requestBody.InnerXml = BuildFailureXml(PropertyTaxBillResult.Error("An error occurred. Please try again.")).OuterXml;
            }

            return inMsg;
        }

        private XmlNode ToXmlResponse(PropertyTaxBillResult result)
        {
            if (result.IsSuccess)
            {
                return BuildSuccessXml(result);
            }
            else
            {
                return BuildFailureXml(result);
            }
        }

        private XmlNode BuildSuccessXml(PropertyTaxBillResult result)
        {
            var inMsg = new XmlDocument();
            var message = BuildMessageNode(inMsg);
            var data = (XmlElement)message.FirstChild;

            var responseWrapper = inMsg.CreateNode(XmlNodeType.Element, "PropertyTaxBillResponse", "urn:mf:response:propertytaxbill");
            var taxBill = inMsg.CreateNode(XmlNodeType.Element, "PropertyTaxBill", "urn:mf:response:propertytaxbill");
            var payload = inMsg.CreateNode(XmlNodeType.Element, "Payload", "urn:mf:response:propertytaxbill");
            payload.InnerText = result.Document;

            taxBill.AppendChild(payload);
            responseWrapper.AppendChild(taxBill);

            data.AppendChild(responseWrapper);

            return message;
        }
        private XmlNode BuildFailureXml(PropertyTaxBillResult result)
        {
            var inMsg = new XmlDocument();
            var message = BuildMessageNode(inMsg);
            var data = (XmlElement)message.FirstChild;

            var errorNode = inMsg.CreateNode(XmlNodeType.Element, "PropertyTaxBillRequestError", "urn:mf:response:propertytaxbillerror");

            errorNode.InnerText = string.Join(",", result.Errors);

            data.AppendChild(errorNode);

            return message;
        }

        private XmlNode BuildMessageNode(XmlDocument inMsg) {
            var message = inMsg.CreateNode(XmlNodeType.Element, "Message", "urn:env:response:v1");
            var dataNode = (XmlElement)inMsg.CreateNode(XmlNodeType.Element, "Data", "urn:env:response:v1");
            dataNode.SetAttribute("encrypted", "no");
            dataNode.SetAttribute("gzip", "no");

            message.AppendChild(dataNode);

            return message;
        }
    }
}
